const { format } = require('winston');
const { combine, timestamp, printf } = format;

const getJSONString = (msg) => (typeof msg === 'string') ? msg : JSON.stringify(msg);

const { LOG_FORMAT = 'json' } = process.env;
const drifterLoggerString = printf(({ level, message, timestamp }) => {
    return `${timestamp} [${process.pid}] ${level}: ${getJSONString(message)}`;
});

const drifterLoggerJSON = printf(({ level, message, timestamp, ...meta }) => {
    return  JSON.stringify({ id: process.pid, timestamp, level, ...meta, message });
});

export const drifterFormat =  combine(
    timestamp(),
    (LOG_FORMAT === 'json') ? format.json(): format.colorize(),
    format.splat(),
    format.ms(),
    (LOG_FORMAT === 'json') ? drifterLoggerJSON : drifterLoggerString
)