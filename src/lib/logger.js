const {createLogger, transports, format} = require('winston');
const path = require('path');
const { drifterFormat } = require('./format')

const { LOGGER_TYPE = 'console,file' } = process.env;
const DRIFTER_PATH = {
    ERROR: path.join(process.cwd(), 'log/error.log'),
    INFO: path.join(process.cwd(), 'log/info.log'),
    COMBINED: path.join(process.cwd(), 'log/combined.log'),
    REJECTION: path.join(process.cwd(), 'log/rejection.log'),
    EXCEPTION: path.join(process.cwd(), 'log/exception.log')
}

//https://github.com/winstonjs/winston
const logger = createLogger({
    transports: [
        // log to the console
        ...(LOGGER_TYPE.includes('console')) ? [
            new transports.Console({
                handleExceptions: true,
                handleRejections: true,
                format: drifterFormat
            })
        ]: [],
        ...(LOGGER_TYPE.includes('file')) ? [
            // log the error to different file
            new transports.File({
                filename: DRIFTER_PATH.ERROR,
                level: 'error',
                format: drifterFormat
            }),
            // log everything to one file
            new transports.File({
                filename: DRIFTER_PATH.COMBINED,
                format: drifterFormat
            })
        ] : []
    ],
    rejectionHandlers: [
        ...(LOGGER_TYPE.includes('file')) ? [
            new transports.File({
                filename: DRIFTER_PATH.REJECTION,
                format: drifterFormat
            })
        ] : []
    ],
    // log an uncaught exception
    exceptionHandlers: [
        ...(LOGGER_TYPE.includes('file')) ? [
            new transports.File({
                filename: DRIFTER_PATH.EXCEPTION,
                format: drifterFormat
            })
        ] : []
    ],
    // don't exit upon uncaught exception
    exitOnError: false
});

const log = logger.info;
module.exports = {logger, log}