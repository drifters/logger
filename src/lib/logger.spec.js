describe('Logger', () => {
  const OLD_ENV = process.env;

  beforeEach(() => {
    jest.resetModules() // this is important - it clears the cache
    process.env = { ...OLD_ENV };
    delete process.env.NODE_ENV;
  });

  afterEach(() => {
    process.env = OLD_ENV;
  });

  it('Logger should log result', () => {
    const { logger, log } = require('./logger')
    log({"a":{"b":1}}, "nasty")
    const childLogger = logger.child({ requestId: '451' });
    const childLogger2 = childLogger.child({ requestId: '452', "genital":"M" });
    childLogger.info("magic penis")
    childLogger2.info("magic penis2")
    logger.error("hello")
    logger.warn("hello")
    logger.info({"c":1}, {"a":1, "b":2})
    // info: test message my string {}
    logger.log('info', 'test message %s', 'my string');

// info: test message 123 {}
    logger.log('info', 'test message %d', 123, {"a":"nasty"});

// info: test message first second {number: 123}
    logger.log('info', 'test message %s, %s %s', 'first', 'second','third', { "a" : 1});
  });

  it('Logger should log result for console only', () => {
    process.env.LOGGER_TYPE = 'console';
    const { logger } = require('./logger')
    logger.log('info', 'test message %s', 'my string');
  })

  it('Logger should log result for file only', () => {
    process.env.LOGGER_TYPE = 'file';
    const { logger } = require('./logger')
    logger.log('info', 'test message %s', 'my string');
  })

  it('Logger should log result in text format', () => {
    process.env.LOG_FORMAT = 'text';
    const { logger } = require('./logger')
    logger.log('info', 'test message %s', 'my string');
    logger.info({"a": 1})
  })

  it('Logger should log result in json format only', () => {
    process.env.LOG_FORMAT = 'json';
    const { logger } = require('./logger')
    logger.log('info', 'test message %s', 'my string');
  })
});
